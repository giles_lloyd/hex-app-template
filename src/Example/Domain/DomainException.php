<?php
declare(strict_types=1);

class DomainException extends RuntimeException
{
	protected $message = "An unknown error occurred.";
}
