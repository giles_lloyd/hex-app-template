<?php
declare(strict_types=1);

namespace Example;

class CreateExampleService
{
	/**
	 * @var ExampleRepository
	 */
	private $exampleRepository;

	public function __construct(ExampleRepository $exampleRepository)
	{
		$this->exampleRepository = $exampleRepository;
	}

	public function handle(CreateExampleRequest $request): ExampleResponse
	{
		$example = Example::factory(#%REQGETTERUSE%
		);
		$this->exampleRepository->add($example);

		return $this->createResponse($example);
	}

	private function createResponse(Example $example): ExampleResponse
	{
		return new ExampleResponse(
			$example->getID(),#%ENTGETTERUSE%
		);
	}
}
