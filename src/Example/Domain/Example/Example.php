<?php
declare(strict_types=1);

namespace Example;

class Example extends \ES\AggregateRoot
{
#%PROPERTY%
	public static function factory(#%PARAMETERS%): Example
	{
		$example = new Example();

		$example->recordThat(
			ExampleCreatedEvent::occur($example->getID(), [
				'id' => $example->getID(),
				'example' => $example,#%EVENTASSIGN%
			])
		);


		return $example;
	}

#%GETTER%
}
