<?php
declare(strict_types=1);

namespace Example;

use Prooph\EventSourcing\AggregateChanged;

class ExampleCreatedEvent extends AggregateChanged
{
	/**
	 * @return string
	 */
	public function getID(): string
	{
		return $this->payload['id'];
	}

	/**
	 * @return Example
	 */
	public function getExample(): Example
	{
		return $this->payload['example'];
	}

#%EVENTGETTER%
}
