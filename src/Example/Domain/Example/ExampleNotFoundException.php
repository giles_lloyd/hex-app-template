<?php
declare(strict_types=1);

namespace Example;

class ExampleNotFoundException extends \DomainException
{
	protected $message = 'Example not found';
}
