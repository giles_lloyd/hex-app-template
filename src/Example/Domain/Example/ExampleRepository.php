<?php
namespace Example;

interface ExampleRepository
{
	public function add(Example $example): void;

	public function getByID($id): Example;

	public function save(Example $example): void;
}
