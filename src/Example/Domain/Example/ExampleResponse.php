<?php
declare(strict_types=1);

namespace Example;

class ExampleResponse
{
	/**
	 * @var string
	 */
	private $id;

#%PROPERTY%
	public function __construct(string $id, #%PARAMETERS%)
	{
		$this->id = $id;
#%ASSIGNS%
	}

	/**
	 * @return string
	 */
	public function getID(): string
	{
		return $this->id;
	}

#%GETTER%
}
