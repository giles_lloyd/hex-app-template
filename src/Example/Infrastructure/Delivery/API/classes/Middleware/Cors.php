<?php
declare(strict_types=1);

namespace Middleware;

use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;

class Cors
{
	public function __invoke(ServerRequestInterface $request, ResponseInterface $response, callable $next): ResponseInterface
	{
		$response = $next($request, $response);

		return $response->withHeader('Access-Control-Allow-Origin', '*');
	}
}
