<?php

const CLI_ROOT = __DIR__;

$autoloader = require_once realpath(__DIR__.'/../../../../../vendor/autoload.php');
require_once realpath(__DIR__.'/../../bootstrap.php');

$autoloader->addPsr4("", CLI_ROOT.'/classes/commands');

require(__DIR__."/bootstrap.php");


if (PHP_SAPI !== 'cli') {
	throw new \Exception("Only invoke this from the CLI");
}

$application = new \Symfony\Component\Console\Application();

//$application->add(new HolidayCommand());

$application->run();
