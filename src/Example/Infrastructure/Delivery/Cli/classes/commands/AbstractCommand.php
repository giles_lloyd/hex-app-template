<?php
declare(strict_types=1);

abstract class AbstractCommand extends \Symfony\Component\Console\Command\Command
{
	/**
	 * @var League\Container\Container
	 */
	protected $di_container;

	/**
	 * @var League\Tactician\CommandBus
	 */
	protected $command_bus;

	public function __construct()
	{
		parent::__construct();

		$this->di_container = \DI\Container::instance();
		$this->command_bus = \CommandBus::instance();
	}
}