<?php
declare(strict_types=1);

namespace Infrastructure\Domain\Example;

use Repository\CQRSRepository;
use Example\Example;
use Example\ExampleNotFoundException;
use Example\ExampleRepository;

class CQRSExampleRepository extends CQRSRepository implements ExampleRepository
{
	const ENTITY_CLASS = Example::class;

	public function add(Example $example): void
	{
		$this->saveAggregateRoot($example);
	}

	public function getByID($id): Example
	{
		if ($example = $this->getAggregateRoot($id)) {
			return $example;
		}

		throw new ExampleNotFoundException();
	}

	public function save(Example $example): void
	{
		$this->saveAggregateRoot($example);
	}
}
