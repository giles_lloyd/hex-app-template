<?php
declare(strict_types=1);

namespace Infrastructure\Domain\Example;

use Domain\Event\DomainEventListener;
use Repository\DoctrineRepository;
use Example\Example;
use Example\ExampleCreatedEvent;
use Example\ExampleNotFoundException;
use Example\ExampleRepository;

class DoctrineExampleRepository extends DoctrineRepository implements DomainEventListener, ExampleRepository
{
	const ENTITY_CLASS = Example::class;

	public function handle($event)
	{
		switch (get_class($event)) {
			case ExampleCreatedEvent::class:
				$this->add($event->getExample());
				break;
			default:
				$this->saveChanges();
		}
	}

	public function add(Example $example): void
	{
		$this->saveNewModelToDB($example);
	}

	public function save(Example $example): void
	{
		$this->saveChanges();
	}

	public function getByID($id): Example
	{
		if ($example = $this->getEntityByID(static::ENTITY_CLASS, $id)) {
			return $example;
		}

		throw new ExampleNotFoundException();
	}
}
