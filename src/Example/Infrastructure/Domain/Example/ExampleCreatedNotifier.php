<?php
declare(strict_types=1);

namespace Infrastructure\Domain\Example;

use Domain\Event\DomainEventListener;
use Message\MessageBus;
use Example\ExampleCreatedEvent;

class ExampleCreatedNotifier implements DomainEventListener
{
	private $messageBus;

	public function __construct(MessageBus $messageBus)
	{
		$this->messageBus = $messageBus;
	}

	/**
	 * @param ExampleCreatedEvent $event
	 */
	public function handle($event)
	{
		$this->messageBus->publish(
			'example',
			[
				'role' => 'example',
				'cmd' => 'created',
				'payload' => [
					'id' => $event->getExample()->getID(),#%MESSAGEGETTERUSE%
				],
			]
		);
	}
}
