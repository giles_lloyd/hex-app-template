<?php

return [
	'dsn'      => 'mysql:host=example-db;dbname=fmf-examples',
	'driver'   => 'pdo_pgsql',
	'user'     => 'fmf-examples',
	'password' => 'password',
	'dbname'   => 'fmf-examples',
	'host'	   => 'example-db',
];
