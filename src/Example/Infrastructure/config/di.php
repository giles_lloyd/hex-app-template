<?php

return [
	'message_bus' => [
		Message\MessageBus::class,
		[
			new Configuration('rabbit'),
		],
		\DI\InjectionType::SHARED
	],
	'write_example_repository' => [
		Infrastructure\Domain\Example\CQRSExampleRepository::class,
		[
			Domain\Event\EventBus::getEventStore(),
			ES\SnapshotStore::instance(),
		],
		\DI\InjectionType::SHARED
	],
	'read_example_repository' => [
		Infrastructure\Domain\Example\DoctrineExampleRepository::class,
		[],
		\DI\InjectionType::SHARED
	],
	'create_example_service' => [
		Example\CreateExampleService::class,
		[
			'write_example_repository',
		]
	],
	'example_created_notifier' => [
		Infrastructure\Domain\Example\ExampleCreatedNotifier::class,
		[
			'message_bus',
		]
	]
];
